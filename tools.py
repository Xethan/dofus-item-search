import numpy
import csv

stats_base_weight = numpy.array([0.2, 3, 1, 1, 1, 1, 0.1, 2, # stats de base
					5, 5, 5, 5, 5, 5, 5, 20, # dommages
					2, 2, 2, 2, 2, 2, 2, # res
					6, 6, 6, 6, 6, # % res
					10, 2, 5, # renvoi dommages / dommages pièges
					7, 7, 7, 7, # esquive / retrait
					3, 0.25, 4, 4, 10, 30, 10, # prospection / pod / fuite / tacle / crit / invoc / soin
					51, 100, 90]) # Po / PA/ PM

def get_stats_weight_multiplier_array(args):
	return [args.vitalite, args.sagesse, args.intelligence, args.force, args.chance, args.agilite, args.initiative, args.puissance,
			args.do_cri, args.do_pou, args.do_neutre, args.do_feu, args.do_terre, args.do_eau, args.do_air, args.do,
			args.re_cri, args.re_pou, args.re_neutre, args.re_feu, args.re_terre, args.re_eau, args.re_air,
			args.re_per_neutre, args.re_per_feu, args.re_per_terre, args.re_per_eau, args.re_per_air,
			args.do_ren, args.do_per_piege, args.do_piege,
			args.re_pa, args.re_pm, args.ret_pa, args.ret_pm,
			args.prospection, args.pod, args.fuite, args.tacle, args.cri, args.invoc, args.soin,
			args.Po, args.PA, args.PM]

def get_stats_weight_vector(args):
	return numpy.vstack(get_stats_weight_multiplier_array(args) * stats_base_weight)

def add_item(args):
	new_item = [args.nom, args.type_obj, args.lvl] + [int(x) for x in get_stats_weight_multiplier_array(args)]
	try:
		with open('items.csv', 'a', newline='') as csvfile:
			csv_writer = csv.writer(csvfile)
			csv_writer.writerow(new_item)
	except OSError as e:
		print(e)