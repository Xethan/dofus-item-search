import numpy
import pandas
import tools
import tools_parsing

def filter_objects(search_parameters):
	filtered_items = pandas.read_csv('items.csv', index_col=False).values
	if search_parameters.nom:
		filtered_items = filtered_items[ [i for i in range(filtered_items.shape[0]) if filtered_items[:, 0][i].find(search_parameters.nom) != -1] ]
	if search_parameters.type_obj:
		filtered_items = filtered_items[ filtered_items[:, 1] == search_parameters.type_obj ]
	filtered_items = filtered_items[filtered_items[:, 2] >= search_parameters.lvl_min]
	filtered_items = filtered_items[filtered_items[:, 2] <= search_parameters.lvl_max]
	return filtered_items

def search_items(search_parameters):
	filtered_items = filter_objects(search_parameters)
	if len(filtered_items) == 0:
		return
	stats_weight_vector = tools.get_stats_weight_vector(search_parameters)
	items_score = filtered_items[:, 3:].astype(float).dot(stats_weight_vector)
	argsort = numpy.hstack(numpy.argsort(items_score, axis=0)[::-1])

	df = pandas.DataFrame(filtered_items[argsort],
			columns=['Nom', 'Type', 'Niveau', 'Vitalité', 'Sagesse', 'Intelligence', 'Force', 'Chance', 'Agilité', 'Initiative', 'Puissance',
			'do_cri', 'do_pou', 'do_neutre', 'do_feu', 'do_terre', 'do_eau', 'do_air', 'do',
			're_cri', 're_pou', 're_neutre', 're_feu', 're_terre', 're_eau', 're_air',
			're_per_neutre', 're_per_feu', 're_per_terre', 're_per_eau', 're_per_air',
			'do_ren', 'do_per_piege', 'do_piege',
			're_pa', 're_pm', 'ret_pa', 'ret_pm',
			'Prospection', 'Pod', 'Fuite', 'Tacle', '% Critique', 'Invocation', 'Soin',
			'Po', 'PA', 'PM'])
	sorted_items = [{key: value for key, value in dic.items() if value != 0 } for dic in df.to_dict(orient='rows')]

	sorted_results = list(zip(numpy.hstack(items_score[argsort]), sorted_items))
	if search_parameters.nb_objets:
		sorted_results = sorted_results[:search_parameters.nb_objets]
	for result in sorted_results:
		print(result[0], ':', result[1])

def main():
	args = tools_parsing.parse_args()
	if args.csv:
		tools.add_item(args)
	else:
		search_items(args)

if __name__ == '__main__':
	main()