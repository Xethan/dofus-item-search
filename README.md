J'ai créé ce petit script Python pour tester mon algorithme de recherche d'objets.

#### Fonctionnalités
* Ajouter des objets à un fichier csv
* Rechercher des objets parmi un fichier csv en prioritarisant les stats qui nous intéressent

#### Détails de l'algorithme
* Les objets peuvent être filtrés par nom, niveau ou type d'objet.
* L'algorithme attribue une note à chaque objet correspondant à la somme de (stat * poids de la stat * multiplicateur donné par l'utilisateur) pour chacune de ses stats.

Exemple :  
Un Anneau Royal du Bouftou qui a pour seule stat 60 de vitalité.  
L'utilisateur veut un multiplicateur de 1.5 sur la vitalité.  
Sa note sera de 60 * 1.5 * 0.2 (poids de 1 de vitalité) = 18
* L'algorithme affiche tous les objets correspondant aux filtres donnés triés par note décroissante.

#### Exemples d'Utilisation
* Recherche d'objet :

	python search_items.py --type Bottes --lvlmin 90 --lvlmax 110 --vita 1 --chance 1 --sagesse 1 --PM 1
![Recherche d'objet](https://bytebucket.org/Xethan/dofus-item-search/raw/2651083479a373d1231c5e20d604f54285b99c03/screenshots_readme/Capture%20d%27ecran%20Recherche%20Objets%20dofus-item-search.png)
* Ajout d'objet :

	python search_items.py --csv --nom 'Super Bottes' --type Bottes --lvl 95 --vita 75 --puissance 25 --PM 1
![Ajout d'objet](https://bytebucket.org/Xethan/dofus-item-search/raw/2651083479a373d1231c5e20d604f54285b99c03/screenshots_readme/Capture%20d%27ecran%20Ajout%20d%27Objet%20dofus-item-search.png)
* Fichier csv créé :
![Fichier csv](https://bytebucket.org/Xethan/dofus-item-search/raw/2651083479a373d1231c5e20d604f54285b99c03/screenshots_readme/Capture%20d%27ecran%20csv%20dofus-item-search.png)

#### Pré-requis
Ce script utilise les librairies Numpy et Pandas, ceci devrait suffire :

	pip install -r requirements.txt